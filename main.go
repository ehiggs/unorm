package main

import (
	"golang.org/x/text/unicode/norm"
	"io"
	"os"
)

func main() {
	wc := norm.NFC.Writer(os.Stdout)
	defer wc.Close()
	io.Copy(wc, os.Stdin)
}
