[![build status](https://gitlab.com/ehiggs/unorm/badges/master/build.svg)](https://gitlab.com/ehiggs/unorm/commits/master)

Read unicode from stdin. Convert it to canonical form and print it to stdout. 

This is intended to be used in command line pipelines where you read in a file and sort it,
etc since coreutils doesn't collate correctly.

This project began when trying to work with Spanish language tweetsand I kept
getting weird results when piping data between command line tools. It turns out
that many unicode glyphs are commonly constructed in many ways so it's important
to normalize the data.
